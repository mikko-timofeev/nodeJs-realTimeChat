#!/usr/bin/env node
'use strict';

var args = require('minimist')(process.argv.slice(2)),

	PORT_HTTP = args.http	|| 80,
	PORT_HTTPS = args.https || 443,
	EMAIL = args.email	|| 'masterTimMi@gmail.com',
	FB_GRAPH = args.fb	|| 'v2.12'

; //console.dir(args);

console.log('HTTP port:', PORT_HTTP);
console.log('HTTPS port:', PORT_HTTPS);

// returns an instance of node-greenlock with additional helper methods
var lex = require('greenlock-express').create(
{
	// set to https://acme-v01.api.letsencrypt.org/directory in production
	server: 'https://acme-v01.api.letsencrypt.org/directory'

	// If you wish to replace the default plugins, you may do so here
	//
	, challenges: { 'http-01': require('le-challenge-fs').create({ webrootPath: '/tmp/acme-challenges' }) }
	, store: require('le-store-certbot').create({ webrootPath: '/tmp/acme-challenges' })

	// You probably wouldn't need to replace the default sni handler
	// See https://git.daplie.com/Daplie/le-sni-auto if you think you do
	//, sni: require('le-sni-auto').create({})

	, approveDomains: approveDomains
});

function approveDomains(opts, certs, cb)
{
	// This is where you check your database and associated
	// email addresses with domains and agreements and such


	// The domains being approved for the first time are listed in opts.domains
	// Certs being renewed are listed in certs.altnames
	if (certs)
	{
		opts.domains = certs.altnames;
	}
	else
	{
		opts.email = EMAIL;
		opts.agreeTos = true;
	}

	// NOTE: you can also change other options such as `challengeType` and `challenge`
	// opts.challengeType = 'http-01';
	// opts.challenge = require('le-challenge-fs').create({});

	cb(null, { options: opts, certs: certs });
}

// handles acme-challenge and redirects to https
require('http').createServer(lex.middleware(require('redirect-https')())).listen(PORT_HTTP, function ()
{
	console.log("Listening for ACME http-01 challenges on", this.address());
});

////////////// app itself /////////////////

var express = require("express");
var app = express();

// handles your app
var secureServer = require('https').createServer(lex.httpsOptions, lex.middleware(app)).listen(PORT_HTTPS, function ()
{
	console.log("Listening for ACME tls-sni-01 challenges and serve app on", this.address());
});

var io = require('socket.io')(secureServer);

io.on('connection', function(socket)
{
	// verify user and send list of friend connections
	socket.on('pointers', function(msg)
	{
		// msg = {from: ..., user: ..., token: ..., friendList: x,y,z...}
	
		// get rows from redis DB matching friend ids using "msg.token" and "msg.friends"
		console.log(new Date(), '|', msg.from,' sent token and asked for pointers');
/*		
		require('https').get('https://graph.facebook.com/' + FB_GRAPH + '/debug_token?input_token='+msg.token, (resp) =>
		{
			let data = '';
		
			// A chunk of data has been recieved.
			resp.on('data', (chunk) => {
				data += chunk;
			});
		
			// The whole response has been received. Print out the result.
			resp.on('end', () => {
				console.log(JSON.parse(data).explanation);
				
				
				var jsonObj = JSON.parse(data);
                                //console.log (jsonObj);
                                if (jsonObj.is_valid && jsonObj.user_id === msg.user)
                                {
                                        // get sessionID + facebookId rows for friends
                                        var matches = []; // array of matches ? maybe object
                                        //// msg.friends.split(',')  //// redis ////
			
                                        socket.emit('pointers',{friendList: matches});
                                        console.log(new Date(),'|', msg.from,' verified, pointers sent');
                                }
                                else
                                {
                                        // socket failed to prove identity
					console.log(new Date(),'|', msg.from,' verification failed');
	                        }
				
			});
		}).on("error", (err) => {
			console.log("Error: " + err.message);
		});
*/	
	});

	socket.on('private', function(msg) // private message to some receiver (room)
	{ // msg = {from: ..., to: ..., message: ...}
		socket.emit('private',{from: msg.from, to: msg.to, message: msg.message});
	});

	socket.on('broadcast', function(msg) // broadcast message to everyone
        { // msg = {from: ..., message: ...}
		console.log(new Date(), '| BROADCAST from', msg.from,': ',msg.message);
		//socket.broadcast.emit('news',{from: msg.from, message: msg.message}); // to everyone except me
		io.sockets.emit('news',{from: msg.from, message: msg.message}); // to absolutely everyone
	});
});

app.use('/', express.static(__dirname + '/client'));
