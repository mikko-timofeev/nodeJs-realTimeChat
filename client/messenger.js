function messengerInit (userId, fbToken) {
	var socket = io();

	//console.log('Socket opts',socket.io.engine);

	window.addEventListener('hashchange',function(){contacts();});
	
	document.getElementById('lastRow').addEventListener('submit',function(){sendMessage();});
	//document.getElementById('send').addEventListener('click',function(){sendMessage();});
	document.getElementById('room').addEventListener('input',function(){document.getElementById('messages').innerHTML='';});

	var messages = document.getElementById('messages'),
	    roomEl = document.getElementById("room");
	
	//document.getElementById('new').focus();

	function sendMessage ()
	{
		var field = document.getElementById('new');
		if (roomEl.value === '')
		{
			socket.emit('broadcast', {from: socket.io.engine.id, message: field.value});
		}
		else
		{
			socket.emit('private', {from: socket.io.engine.id, to: roomEl.value, message: field.value});
		}
		field.value = '';
        }

	// after connection
	socket.on('connect', function()
	{
		//console. log ('User id:', userId);
		//console. log ('FB Token:', fbToken);
		
		roomClean ();

		roomUpdate ('', "BROADCAST");
		//roomUpdate (socket.io.engine.id, "Me");

		console.log('Session id:',socket.io.engine.id);
		socket.emit ('pointers', {from: socket.io.engine.id, token: fbToken, friendList: sessionStorage.getItem('contacts')});
	});

	socket.on('pointers', function(msg)
	{
		console.log('Friend list updated');
		console.log(msg);
	});

	socket.on('private', function(msg)
	{
		//console.log('Private:',msg);
		recieve(msg);
	}); // private message
	socket.on('news', function(msg)
	{
		//console.log('BROADCAST:',msg);
		recieve(msg);
	}); // BROADCAST

	function recieve(msg)
	{
		var messageClass = (socket.io.engine.id === msg.from)? 'me': msg.from,
			friendId = '',
			senderIs = 'Broadcaster';
		
		if (messageClass == 'me')
		{
			senderIs = 'Me';
		}
		else
		{
			var knownSenderName = sessionStorage. getItem ('pointer[' + msg.from + ']');

			if (knownSenderName != null)
			{
				senderIs = sessionStorage. getItem ('person[' + knownSenderName + ']');
			}
		}

		var newMessage = document.createElement('LI');

		var sender = document.createElement('BUTTON');
		sender.className = 'mSender';
		sender.innerHTML = senderIs;

		var text = document.createElement('SPAN');
		text.className='mText';
		text.innerText = msg.message;
		var timestamp = document.createElement ('TIME');
		timestamp.className='mTime';
		timestamp.dateTime=new Date();
		timestamp.innerText='\n'+timestamp.dateTime;
		text.appendChild (timestamp);

		newMessage.className=messageClass;

		if (senderIs === 'Me')
		{
			newMessage.appendChild(sender);
	                newMessage.appendChild(text);
		}
		else
		{
			newMessage.appendChild(text);
			newMessage.appendChild(sender);
		}
		
		//messages.appendChild(newMessage);
		messages.insertBefore(newMessage,messages.childNodes[0]);

		sender.onclick=function(){roomUpdate(msg.from,senderIs)};
        }
	
	function roomClean ()
	{
		roomEl.innerHTML = '';
		history.pushState (null, null, '#');
	}

	function roomUpdate (sessionId, name)
	{
                var option = document.createElement("option");
                option.text = name + ' [' + sessionId + ']';
                option.value = sessionId;
                roomEl.add(option);
		option.selected = true;
		history.pushState (null, null, '#'+sessionId);
	}

	function contacts ()
	{
		
		roomEl.value=window.location.hash.substring(1);
	}
}
