/* Facebook's stuff: */
/*
(function (d, s, id)
{
	var appId = 1812074362413175;
	var apiVer = '2.12'; // https://developers.facebook.com/docs/graph-api/changelog

	var js, fjs = d. getElementsByTagName (s) [0];
	if (d. getElementById (id)) return;
	js = d. createElement (s);
	js. id = id;
	js. src = "//connect.facebook.net/en_GB/sdk.js#cookie=0&xfbml=1&version=v"+apiVer+"&appId="+appId;
	fjs. parentNode. insertBefore (js, fjs);
}(document, 'script', 'facebook-jssdk'));
*/
messengerInit('','');

window. fbAsyncInit = function ()
{
	checkLoginState();
}

function checkLoginState ()
{
	FB. getLoginStatus
	(
		function (response)
		{
			var checker = setInterval(function(){
				if (response. status === 'connected')
				{
					//userData_load ();
					clearInterval (checker);
				}
			}, 50);

			statusChangeCallback (response);

			//window. location. reload ();
		}
	);
}

function statusChangeCallback (response)
{
	if (response.status === 'connected') {
		// the user is logged in and has authenticated your
		// app, and response.authResponse supplies
		// the user's ID, a valid access token, a signed
		// request, and the time the access token
		// and signed request each expire

		var uid = response.authResponse.userID;
		var accessToken = response.authResponse.accessToken;

		//window. parent. sessionStorage. setItem ('myToken', response.authResponse.accessToken);
		//alert ('uid: '+uid);
		//alert ('accessToken: '+accessToken);
		FB. api
		(
			'/me',
			'GET',
			{"fields":"first_name,last_name,id,locale,timezone,cover"},
			function (response)
			{
				//console.log(response);
				sessionStorage. setItem ('myId', response. id);
				sessionStorage. setItem ('myName', response. first_name);
				sessionStorage. setItem ('mySurname', response. last_name);
				sessionStorage. setItem ('myLocale', response. locale);
				//sessionStorage. setItem ('myCover', response. cover. source);
				//console.log (sessionStorage. getItem ('myCover'))

				messengerInit (uid, accessToken);
			}
		);

		//'https://graph.facebook.com/me/friends?fields=installed'
		FB. api
		(
			'/me/friends',
			function(response)
			{
				var fellowUsers = response. data. length;
				//friendList = ['<tr><td>['+fellowUsers+']</td><td>Friends</td></tr>'];
				var contacts = [];
				//alert ('123')
				for (var i=0; i < fellowUsers; i+=1)
				{
					contacts. push (response. data [i]. id);
					sessionStorage. setItem ('person[' + response. data [i]. id + ']', response. data 
[i]. name);
				}
				sessionStorage. setItem ('contacts', contacts. join ());
			}
		);


		document.getElementById('reply').className = 'authorised';
		
	} else if (response. status === 'not_authorized') {
		// the user is logged in to Facebook,
		// but has not authenticated your app
		clearOut ();

		document.getElementById('reply').className = 'unauthorised';
		
	} else {
		// the user isn't logged in to Facebook.
		clarOut ();
		
		document.getElementById('reply').className = 'loggedout';
	}
} 

function clearOut ()
{
	if (sessionStorage. length !== 0)
        {
	        sessionStorage. clear ();
                roomClean ();
                //document.getElementById('messages').innerHTML='';
		
		document.getElementById (''). reset ();
         }
	messengerInit ('', '');
}
